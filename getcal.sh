#!/usr/bin/env bash

mkdir data/cals

Crea_BIB=$(date -d "2013-04-30" +%s)
End_events=$(date -d "now + 3 years" +%s)
CalURL=$(echo "https://cloud.lebib.org/remote.php/dav/public-calendars/5MMjE3n4ToxDKX8p/?export&accept=jcal&expand=1&start=$Crea_BIB&end=$End_events")
CalURLnoexp=$(echo "https://cloud.lebib.org/remote.php/dav/public-calendars/5MMjE3n4ToxDKX8p/?export&accept=jcal")

curl $CalURLnoexp | jq -f <(echo '
def objectify: map({key: .[0], value: .[1:]}) | from_entries;

if .[0] == "vcalendar" then
{
    options: .[1] | objectify,

    cal: {
        tz: .[2] | map(select(.[0] == "vtimezone") | .[1:])
                 | flatten(1)
                 | map(objectify),

        events: .[2] | map(select(.[0] == "vevent") | .[1])
                     | map(objectify),
    }
}
else "Error: Not a vcalendar" end') > data/cals/cal_noexp.json

curl $CalURL | jq --argfile noexp data/cals/cal_noexp.json -f <(echo '
def objectify: map({key: .[0], value: .[1:]}) | from_entries;

INDEX($noexp.cal.events[]; .uid[2]) as $edict |
if .[0] == "vcalendar" then
{
    options: .[1] | objectify,

    cal: {
        tz: .[2] | map(select(.[0] == "vtimezone") | .[1:])
                 | flatten(1)
                 | map(objectify),

        events: .[2] | map(select(.[0] == "vevent") | .[1])
                     | map(objectify)
                     | map( $edict[.uid[2]] + . )
		     | sort_by(.dtstart[2]),
    }
}
else "Error: Not a vcalendar" end') > data/cals/cal.json

rm data/cals/cal_noexp.json
