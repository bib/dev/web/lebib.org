**!!! Déplacé sur https://codeberg.org/bib/lebib.org !!!**

# LeBIB.org

Documentation, création et développement du site du BIB

## Fonctionnement

Voir les Issues pour s'organiser voir les tâches à faire. Compte-rendu de réunions dans le dossier `cr_reu`

## Instructions

### Déploiement en ligne automatisé ("CI")

**Adresse :** http://bib.frama.io/dev/web/lebib.org

TODO: Décrire le principe du "CI"

### Déploiement en local, sur ta machine

Pour participer au développement il faut avoir un compte framagit dans le groupe bib, avec idéalement une clé ssh configurée.

On peut aller cloner le dépôt et local, puis rentrer dans son dossier :
``` sh
$ git clone git@framagit.org:bib/dev/web/lebib.org.git
$ cd lebib.org
```

Il faut alors executer le script `./getcal.sh`. Il importera la dernière version de l'agenda dans `data/cal.json`

``` sh
$ ./getcal.sh
```

On peut à ce moment modifier le site comme on le souhaite.

Pour compiler le site et produire les fichiers dans `public/` :

``` sh
$ hugo
```

Ou pour lancer un serveur local :

``` sh
$ hugo server
```

Le site est alors accessible en local sur : `localhost:1313/dev/web/lebib.org/`

