---
title: Laboratoire
css_style: lab
description: "Un laboratoire de microbiologie"
---

Becs benzène et hottes, microscopes, piepettes, étuves, verrerie, centrifugeuse et plus encore.

Ateliers contraception testiculaire et spermogramme, culture de champignons.
