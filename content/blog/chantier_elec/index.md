---
title: "Chantier électricité au BIB"
description: "Remise à niveau de toute l'infrastructure électrique du lieu"
---

# Plan du chantier
Accessible {{< ext_link title="ici" url="https://framagit.org/bib/inf/phy/-/tree/master/chantier-elec" >}}
